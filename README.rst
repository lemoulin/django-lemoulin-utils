===============
Le Moulin Utils
===============

A collection of utilities for Django.

.. contents::

Getting started
===============

Requirements
------------

Le Moulin Utils requires:

- Python 3.4
- Django 1.7 or greater

Installation
------------

You can get Le Moulin Utils by using pip::

    $ pip install -e git+https://bitbucket.org/lemoulin/django-lemoulin-utils.git#egg=lemoulin_utils

To enable `lemoulin_utils` in your project you need to add it to `INSTALLED_APPS` in your projects
`settings.py` file::

    INSTALLED_APPS = (
        ...
        'lemoulin_utils',
        ...
    )

Usage
=====

Cache
-----

Admin View
**********

Le Moulin Utils offers an admin view to clear your cache. To use it, simply add it to your `urls.py` file::

    urlpatterns = patterns(
        ...
        url(r'^cache/', include("lemoulin_utils.cache.urls", namespace="cache")),
        ...
    )

The view can now be called by::

    http://yourdomain.com/cache/clear/


Management Command
******************

Le Moulin Utils offers a management command to clear your cache. To use it, call::

    $ python manage.py clear_cache
