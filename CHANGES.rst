===============
Le Moulin utils
===============

*******
CHANGES
*******

.. contents:: Versions

v0.1
====
Initial release

v0.2
====
Python 3+

v0.3
====
New Packaging

v0.32
=====
locale_url removed