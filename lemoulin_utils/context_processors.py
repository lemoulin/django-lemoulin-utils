from __future__ import unicode_literals

import socket
from django.conf import settings
from django.core.cache import get_cache

from . import datetime

try:
	HOSTNAME = socket.gethostname()
except:
	HOSTNAME = 'localhost'


def common_settings(request):
	"""Passing custom CONSTANT in Settings into RequestContext."""
	from django.contrib.sites.models import get_current_site

	cache_stats = []

	for cache_backend_nm, cache_backend_attrs in settings.CACHES.items():
		try:
			cache_backend = get_cache(cache_backend_nm)
			this_backend_stats = cache_backend._cache.get_stats()

			for server_name, server_stats in this_backend_stats:
				cache_stats.append(("%s: %s" % (
					cache_backend_nm, server_name), server_stats))

		except AttributeError: # this backend probably doesn't support that
			continue

	COMMON_CONTEXT = {
		"DEBUG": settings.DEBUG,
		"HOSTNAME": HOSTNAME,
		"NOW": datetime.tz_now(),
		"CURRENT_DOMAIN": get_current_site(request).domain,
		"GOOGLE_ANALYTICS_ACCOUNT": getattr(settings, "GOOGLE_ANALYTICS_ACCOUNT", ""),
		"CACHE_STATS": cache_stats
	}

	try:
		# set EXTRA_CONTEXT in local settings
		COMMON_CONTEXT.update(settings.EXTRA_CONTEXT)
	except:
		pass

	return COMMON_CONTEXT
