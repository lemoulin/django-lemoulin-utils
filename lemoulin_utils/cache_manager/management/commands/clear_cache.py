from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.management.base import NoArgsCommand


class Command(NoArgsCommand):
	help = u"Clears the cache"

	def handle_noargs(self, **options):
		from django.core.cache import cache
		cache.clear()

		self.stdout.write(_("Cache cleared"))
