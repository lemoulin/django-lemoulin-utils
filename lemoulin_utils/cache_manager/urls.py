from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import ClearCacheView

urlpatterns = patterns(
	'',
	url(r'^clear/$', ClearCacheView.as_view(), name="clear"),
)
