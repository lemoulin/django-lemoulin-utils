from __future__ import unicode_literals

from django.conf import settings
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.cache import never_cache

from lemoulin_utils.datetime import tz_now

class ClearCacheView(TemplateView):
	template_name = "cache_manager/admin/clear_cache.html"

	@method_decorator(staff_member_required)
	@method_decorator(never_cache)
	def dispatch(self, request, *args, **kwargs):
		from django.core.management import call_command

		call_command("clear_cache", verbosity=3, interactive=False)

		return super(ClearCacheView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		import socket

		context = super(ClearCacheView, self).get_context_data(**kwargs)

		try:
			HOSTNAME = socket.gethostname()
		except:
			HOSTNAME = 'localhost'

		context["DEBUG"] = settings.DEBUG
		context["HOSTNAME"] = HOSTNAME
		context["NOW"] = str(tz_now())

		return context
