from __future__ import unicode_literals

def clean_filename(filename):
	from django.utils.text import get_valid_filename, slugify

	try:
		name, extension = filename.rsplit(".", 1)
		separator = "."
	except:
		name = filename
		extension = ""
		separator = ""

	slugified_name = "{name}{separator}{extension}".format(name=slugify(name), separator=separator, extension=extension)
	return get_valid_filename(slugified_name)
