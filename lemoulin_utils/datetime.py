from __future__ import unicode_literals

from django.utils import timezone
from django.utils.dateparse import parse_datetime

def tz_now():
	return timezone.localtime(timezone.now())

def tz_today():
	return tz_now().date()

def tz_aware_datetime(str_datetime, tzinfo=None):
	if not tzinfo:
		tzinfo = tz_now().tzinfo

	return parse_datetime(str_datetime).replace(tzinfo=tzinfo)
