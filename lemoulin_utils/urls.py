from __future__ import unicode_literals

from django.conf.urls import include, patterns, url

urlpatterns = patterns(
	'',
	url(r'^cache/', include("lemoulin_utils.cache_manager.urls", namespace="cache")),
)
