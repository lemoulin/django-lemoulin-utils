from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from optparse import make_option


class Command(BaseCommand):
	args = "--username [username] --email [email] --password [password]"
	help = _("Creates a super user without interaction")

	option_list = BaseCommand.option_list + (
		make_option("--username",
			help=_("Username")),
		make_option("--email",
			help=_("Email")),
		make_option("--password",
			help=_("Password")),
	)

	def handle(self, *args, **options):
		username = options["username"]
		email = options["email"]
		password = options["password"]

		if not options.get("username", None):
			print("--username argument is required")
		elif not options.get("email", None):
			print("--email argument is required")
		elif not options.get("password", None):
			print("--password argument is required")
		else:
			user = get_user_model()._default_manager.create_superuser(username=username, email=email, password=password)

			print("User created")

