from __future__ import unicode_literals

from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def current_params(context, ):
	request = context["request"]

	params = ""
	for key, value in request.GET.iteritems():
		if params:
			separator = "&"
		else:
			separator = ""
		params += "{separator}{key}={value}".format(separator=separator, key=key, value=value)

	if params:
		params = "?{params}".format(params=params)

	return params
