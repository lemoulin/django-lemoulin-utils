from __future__ import unicode_literals

from django import template

register = template.Library()


@register.filter
def get_item(dictionary, key):
	"""
	returns a specific dictionary item

	Usage:
	{{ my_dict|get_item:key}}
	"""
	return dictionary.get(key)


@register.filter
def key(dict_obj, position):
	"""
	returns a specific dictionary key

	Usage:
	{{ my_dict|key:2}}
	"""
	try:
		return dict_obj.keys()[position]
	except:
		return ""


@register.filter
def value(dict_obj, position):
	"""
	returns a specific dictionary value

	Usage:
	{{ my_dict|value:2}}
	"""
	try:
		return dict_obj.values()[position]
	except:
		return ""


@register.filter
def split(str,splitter):
	return str.split(splitter)
