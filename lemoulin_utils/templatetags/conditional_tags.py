from __future__ import unicode_literals

from django import template

register = template.Library()


def contains(value, arg):
	"""
	Usage:
	{% if link_url|contains:"http://www.youtube.com/" %}
	Stuff
	{% else %}
	Not stuff
	{% endif %}
	"""
	return arg in value

register.filter('contains', contains)


def startswith(value, arg):
	"""
	Usage:
	{% if link_url|contains:"http://www.youtube.com/" %}
	Stuff
	{% else %}
	Not stuff
	{% endif %}
	"""
	return value.startswith(arg)
	return arg in value

register.filter('startswith', startswith)


def endswith(value, arg):
	"""
	Usage:
	{% if link_url|contains:"http://www.youtube.com/" %}
	Stuff
	{% else %}
	Not stuff
	{% endif %}
	"""
	return value.endswith(arg)
	return arg in value

register.filter('endswith', endswith)
