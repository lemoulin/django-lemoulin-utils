from __future__ import unicode_literals

from django.template import Library
import HTMLParser
import re, htmlentitydefs

register = Library()


@register.filter
def entities_to_unicode(value):
	return re.sub('&([^;]+);', lambda m: unichr(htmlentitydefs.name2codepoint[m.group(1)]), value)


@register.filter
def truncatewords_by_chars(value, arg):
	"""Truncate the text when it exceeds a certain number of characters.
	Delete the last word only if partial.
	Adds '...' at the end of the text.

	Example:

		{{ text|truncatewords_by_chars:25 }}
	"""
	try:
		length = int(arg)
	except ValueError:
		return value

	if len(value) > length:
		if value[length:length + 1].isspace():
			return value[:length].rstrip() + '...'
		else:
			return value[:length].rsplit(' ', 1)[0].rstrip() + '...'
	else:
		return value


tag_end_re = re.compile(r'(\w+)[^>]*>')
entity_end_re = re.compile(r'(\w+;)')

@register.filter
def truncatehtml_by_chars(string, length, ellipsis='...'):
	"""Truncate HTML string, preserving tag structure and character entities."""
	length = int(length)
	output_length = 0
	i = 0
	pending_close_tags = {}
	h = HTMLParser.HTMLParser()
	string = h.unescape(string)

	while output_length < length and i < len(string):
		c = string[i]

		if c == '<':
			# probably some kind of tag
			if i in pending_close_tags:
				# just pop and skip if it's closing tag we already knew about
				i += len(pending_close_tags.pop(i))
			else:
				# else maybe add tag
				i += 1
				match = tag_end_re.match(string[i:])
				if match:
					tag = match.groups()[0]
					i += match.end()

					# save the end tag for possible later use if there is one
					match = re.search(r'(</' + tag + '[^>]*>)', string[i:], re.IGNORECASE)
					if match:
						pending_close_tags[i + match.start()] = match.groups()[0]
				else:
					output_length += 1 # some kind of garbage, but count it in

		elif c == '&':
			# possible character entity, we need to skip it
			i += 1
			match = entity_end_re.match(string[i:])
			if match:
				i += match.end()

			# this is either a weird character or just '&', both count as 1
			output_length += 1
		else:
			# plain old characters

			skip_to = string.find('<', i, i + length)
			if skip_to == -1:
				skip_to = string.find('&', i, i + length)
			if skip_to == -1:
				skip_to = i + length

			# clamp
			delta = min(skip_to - i,
						length - output_length,
						len(string) - i)

			output_length += delta
			i += delta

	output = [string[:i]]
	if output_length == length:
		output.append(ellipsis)

	for k in sorted(pending_close_tags.keys()):
		output.append(pending_close_tags[k])

	return "".join(output)

truncatehtml_by_chars.is_safe = True
