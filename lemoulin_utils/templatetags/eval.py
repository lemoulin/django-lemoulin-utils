from __future__ import unicode_literals

from django import template

register = template.Library()


@register.tag(name='eval')
def do_eval(parser, token):
	"""Usage: {% eval %}1 + 1{% endeval %}"""

	nodelist = parser.parse(('endeval',))
	parser.delete_first_token()

	return EvalNode(nodelist)


class EvalNode(template.Node):
	def __init__(self, nodelist):
		self.nodelist = nodelist

	def render(self, context):
		string = self.nodelist.render(context)
		rendered_string = template.Template("{{ %s }}" % (eval(string))).render(context)

		if rendered_string and (rendered_string != "None"):
			return rendered_string
		else:
			return ""
