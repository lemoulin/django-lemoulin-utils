from __future__ import unicode_literals

from django import template
from lemoulin_utils import html as lemoulin_html

register = template.Library()


@register.filter
def unescape_html(string):
	return lemoulin_html.unescape(string=string)
