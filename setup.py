from setuptools import setup, find_packages

version = __import__('lemoulin_utils').__version__

packages = find_packages()

setup(
    name='django-lemoulin-utils',
    packages=packages,
    version=version,
    description='A collection of utilities for Django.',
    author='Yanik Proulx',
    author_email='yanikproulx@lemoulin.co',
    url='http://bitbucket.org/lemoulin/django-lemoulin-utils',
    download_url='https://bitbucket.org/lemoulin/django-lemoulin-utils',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    scripts=[],
    license='LICENSE.txt',
    long_description=open('README.rst').read(),
    install_requires=[
        "Django >= 1.7",
    ],
    include_package_data = True,
)
